using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace Actor.Analyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class ActorAnalyzerAnalyzer : DiagnosticAnalyzer
    {
        private static readonly HashSet<string> EmbeddedTypes = new HashSet<string>(new string[]
        {
            "short",
            "int",
            "long",
            "ushort",
            "uint",
            "ulong",
            "float",
            "double",
            "bool",
            "byte",
            "sbyte",
            "decimal",
            "char",
            "System.Guid",
            "System.TimeSpan",
            "System.DateTime",
            "System.DateTimeOffset",
        });

        public const string MethodNeedFuncAttributeId = "AddFuncAttribute";
        public const string MethodParameterOnlyOneId = "MethodParameterError";
        public const string NotSupportReturnTypeId = "MethodReturnTypeError";
        internal const string Category = "Usage";

        private static INamedTypeSymbol funcAttribute { get; set; }
        private static INamedTypeSymbol actorBase { get; set; }
        private static INamedTypeSymbol iActor { get; set; }
        private static INamedTypeSymbol task { get; set; }

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get ; } = ImmutableArray.Create(
            MethodNeedFuncAttribute,
            MethodParameterOnlyOne,
            NotSupportReturnType
            );

        internal static readonly DiagnosticDescriptor MethodNeedFuncAttribute = new DiagnosticDescriptor(
            id: MethodNeedFuncAttributeId,
            title: "Must be marked with FuncAttribute",
            category: Category,
            messageFormat: "Actor methods require either FuncAttribute.",
            description: "Actor method must be marked with FuncAttribute.",
            defaultSeverity: DiagnosticSeverity.Error,
            isEnabledByDefault: true
            );

        internal static readonly DiagnosticDescriptor MethodParameterOnlyOne = new DiagnosticDescriptor(
            id: MethodParameterOnlyOneId,
            title: "Only supports one parameter",
            category: Category,
            messageFormat: "The interface method that inherits IActor only supports one parameter.",
            description: "Actor method only supports one parameter.",
            defaultSeverity: DiagnosticSeverity.Error,
            isEnabledByDefault: true
            );

        internal static readonly DiagnosticDescriptor NotSupportReturnType = new DiagnosticDescriptor(
            id: MethodParameterOnlyOneId,
            title: "Not support return type",
            category: Category,
            messageFormat: "Actor return type is not supported.",
            description: "Actor method does not support return type.",
            defaultSeverity: DiagnosticSeverity.Error,
            isEnabledByDefault: true
            );

        public override void Initialize(AnalysisContext context)
        {
            context.EnableConcurrentExecution();
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.RegisterCompilationStartAction(ctxt =>
            {
                funcAttribute = ctxt.Compilation.GetTypeByMetadataName("Actor.Net.FuncAttribute");
                actorBase = ctxt.Compilation.GetTypeByMetadataName("Actor.Net.ActorBase");
                iActor = ctxt.Compilation.GetTypeByMetadataName("Actor.Net.IActor");
                task = ctxt.Compilation.GetTypeByMetadataName("System.Threading.Tasks.Task");
                ctxt.RegisterSyntaxNodeAction(c => AnalyzeSymbol(c), SyntaxKind.ClassDeclaration, SyntaxKind.InterfaceDeclaration);
            });
        }

        private static void AnalyzeSymbol(SyntaxNodeAnalysisContext context)
        {
            if (context.Node.Kind() != SyntaxKind.ClassDeclaration &&
                context.Node.Kind() != SyntaxKind.InterfaceDeclaration)
                return;

            TypeDeclarationSyntax typeDeclaration = (TypeDeclarationSyntax)context.Node;
            INamedTypeSymbol declaredSymbol = context.SemanticModel.GetDeclaredSymbol(typeDeclaration);
            if (declaredSymbol is null)
                return;

            if (declaredSymbol == null)
                return;

            if (EmbeddedTypes.Contains(declaredSymbol.ToString()))
                return;

            if (declaredSymbol.TypeKind == TypeKind.Enum)
                return;

            if (declaredSymbol.IsGenericType)
                return;

            if (declaredSymbol.Locations[0].IsInMetadata)
                return;

            var allInterfaces = declaredSymbol.AllInterfaces;
            if (allInterfaces == null && !allInterfaces.Any())
                return;

            if (declaredSymbol.TypeKind == TypeKind.Interface)
            {
                if (!allInterfaces.Where(inf => SymbolEqualityComparer.Default.Equals(inf, iActor)).Any())
                    return;

                AnalyzeInterfaceSymbol(context, declaredSymbol);
            }
            else if(declaredSymbol.TypeKind == TypeKind.Class)
            {
                if (!SymbolEqualityComparer.Default.Equals(declaredSymbol.BaseType, actorBase))
                    return;

                var actorInterface = allInterfaces.Where(inf => inf.AllInterfaces.Contains(iActor) && !SymbolEqualityComparer.Default.Equals(declaredSymbol, iActor)).FirstOrDefault();
                if (actorInterface == null)
                    return;

                AnalyzeClassSymbol(context, declaredSymbol, actorInterface);
            }
        }

        /// <summary>
        /// 分析继承IActor interface
        /// </summary>
        /// <param name="context"></param>
        /// <param name="declaredSymbol"></param>
        private static void AnalyzeInterfaceSymbol(SyntaxNodeAnalysisContext context, INamedTypeSymbol declaredSymbol)
        {
            var interfaceMethods = declaredSymbol.GetMembers().OfType<IMethodSymbol>();
            foreach (IMethodSymbol method in interfaceMethods)
            {
                if(method.Parameters == null || method.Parameters.Length != 1)
                {
                    ImmutableDictionary<string, string> typeInfo = ImmutableDictionary.Create<string, string>().Add("type", method.ToString());
                    var diagnostic = Diagnostic.Create(MethodParameterOnlyOne, method.Locations[0], typeInfo);
                    context.ReportDiagnostic(diagnostic);
                    return;
                }

                var rtStr = method.ReturnType.ToString();
                if (SymbolEqualityComparer.Default.Equals(method.ReturnType.BaseType, task)
                    || SymbolEqualityComparer.Default.Equals(method.ReturnType, task)
                    || rtStr == "void")
                {
                    continue;
                }
                else
                {
                    ImmutableDictionary<string, string> typeInfo = ImmutableDictionary.Create<string, string>().Add("type", method.ToString());
                    var diagnostic = Diagnostic.Create(NotSupportReturnType, method.Locations[0], typeInfo);
                    context.ReportDiagnostic(diagnostic);
                }
            }
        }

        /// <summary>
        /// 分析实现Actor class
        /// </summary>
        /// <param name="context"></param>
        /// <param name="declaredSymbol"></param>
        /// <param name="interfaceSymbol"></param>
        private static void AnalyzeClassSymbol(SyntaxNodeAnalysisContext context, INamedTypeSymbol declaredSymbol, INamedTypeSymbol interfaceSymbol)
        {
            var interfaceMethods = interfaceSymbol.GetMembers().OfType<IMethodSymbol>().Where(m => m.Parameters != null && m.Parameters.Length == 1);
            if (!interfaceMethods.Any())
                return;

            var classMethods = declaredSymbol.GetMembers().OfType<IMethodSymbol>();
            foreach (IMethodSymbol method in classMethods)
            {
                if (method.Parameters == null)
                    continue;

                if (method.Parameters.Length != 1)
                    continue;

                var interfaceMethod = interfaceMethods.Where(m => m.Name == method.Name && SymbolEqualityComparer.Default.Equals(m.Parameters[0].Type, method.Parameters[0].Type)).FirstOrDefault();
                if (interfaceMethod == null)
                    continue;

                var funcAttr = method.GetAttributes().FirstOrDefault(x => SymbolEqualityComparer.Default.Equals(x.AttributeClass, funcAttribute));
                if(funcAttr == null)
                {
                    ImmutableDictionary<string, string> typeInfo = ImmutableDictionary.Create<string, string>().Add("type", method.ToString());
                    var diagnostic = Diagnostic.Create(MethodNeedFuncAttribute, method.Locations[0], typeInfo);
                    context.ReportDiagnostic(diagnostic);
                    return;
                }

                AnalyzeFuncAttributeSymbol(context, declaredSymbol, method, funcAttr);
            }
        }

        /// <summary>
        /// 分析FuncAttribute
        /// </summary>
        /// <param name="context"></param>
        /// <param name="methodSymbol"></param>
        /// <param name="funcAttr"></param>
        private static void AnalyzeFuncAttributeSymbol(SyntaxNodeAnalysisContext context, INamedTypeSymbol declaredSymbol, IMethodSymbol methodSymbol, AttributeData funcAttr)
        {
            var funcAttributeString = funcAttr.ToString();
            var returnString = methodSymbol.ReturnType.ToString();
            var parameter = methodSymbol.Parameters[0];
            bool isReport = false;
            //"Actor.Net.FuncAttribute(typeof(Actor.Net.ActorParameter<First.Modles.FirstParameter, First.Modles.FirstParameter>))"
            if (SymbolEqualityComparer.Default.Equals(methodSymbol.ReturnType.BaseType, task))
            {
                returnString = returnString.Replace("System.Threading.Tasks.Task<", string.Empty);
                returnString = returnString.Replace(">", string.Empty);
                var declaredString = $"Actor.Net.FuncAttribute(typeof(Actor.Net.ActorParameter<{returnString}, {parameter}>))";
                isReport = funcAttributeString != declaredString;
            }
            //"Actor.Net.FuncAttribute(typeof(Actor.Net.ActorParameter<First.Modles.FirstParameter>))"
            if (SymbolEqualityComparer.Default.Equals(methodSymbol.ReturnType, task) || returnString == "void")
            {
                var declaredString = $"Actor.Net.FuncAttribute(typeof(Actor.Net.ActorParameter<{parameter}>))";
                isReport = funcAttributeString != declaredString;
            }

            if (isReport)
            {
                ImmutableDictionary<string, string> typeInfo = ImmutableDictionary.Create<string, string>().Add("type", methodSymbol.ToString());
                var diagnostic = Diagnostic.Create(MethodNeedFuncAttribute, methodSymbol.Locations[0], typeInfo);
                context.ReportDiagnostic(diagnostic);
            }
        }

    }
}
