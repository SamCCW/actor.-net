﻿using Actor.Net;
using System;

namespace Location.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            ActorLocationServer.ListenPort = 48798;
            ActorLocationServer.Instance.ExceptionHandler = (cx, ex) => Console.WriteLine(ex.ToString());
            ActorLocationServer.Instance.Start(true);
            Console.ReadKey();
        }
    }
}