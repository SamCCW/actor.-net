﻿using Actor.Net.Location;
using Actor.Net.Network.Actor;
using System;
using System.Threading.Tasks;

namespace Actor.Net
{
    /// <summary>
    /// Actor服务提供类
    /// </summary>
    public class ActorProvider
    {
        /// <summary>
        /// Actor位置服务客户端
        /// </summary>
        internal ActorLocationClient LocationClient => ActorLocationClient.Instance;
        /// <summary>
        /// 本地Actor服务
        /// </summary>
        internal LocalActorServer LocalActorServer { get; set; }
        /// <summary>
        /// 未捕获异常处理者
        /// </summary>
        internal Action<ActorChannelContext, Exception> ExceptionHandler { get; set; }
        /// <summary>
        /// Actor启动成功处理者
        /// </summary>
        internal Action AddActorStartHandler { get; set; }

        /// <summary>
        /// 启动Actor服务提供
        /// </summary>
        /// <param name="blockCurrentThread">是否阻塞当前线程</param>
        public void Start(bool blockCurrentThread)
        {
            if (LocalActorServer == null)
                return;

            LocalActorServer.Listen();

            LocationClient.AddLocalActorServer(LocalActorServer);
            LocationClient.AddExceptionHandler(ExceptionHandler);
            LocationClient.AddActorStartHandler(AddActorStartHandler);
            LocationClient.Connect();

            ActorNetwork.Start(blockCurrentThread);
        }
    }
}
