﻿using System;

namespace Actor.Net
{
    /// <summary>
    /// Actor路由特性,如果在一个接口中有存在方法重写，需要使用路由特性指定重载方法路由
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ActorRouterAttribute : Attribute
    {
        /// <summary>
        /// 路由
        /// </summary>
        public string Router { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="router"></param>
        public ActorRouterAttribute(string router)
        {
            this.Router = router;
        }
    }
}
