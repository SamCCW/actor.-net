﻿using Actor.Net.Location;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Actor.Net
{
    /// <summary>
    /// Actor服务提供静态类
    /// </summary>
    public static class ActorProviderService
    {
        /// <summary>
        /// 位置服务IP地址
        /// </summary>
        internal static string LocationIpAddress { get; set; }
        /// <summary>
        /// 位置服务端口号
        /// </summary>
        internal static int LocationPort { get; set; }
        /// <summary>
        /// 本地Actor服务IP地址
        /// </summary>
        internal static string LocalIpAddress { get; set; }
        /// <summary>
        /// 本地Actor服务端口号
        /// </summary>
        internal static int LocalPort { get; set; }
        /// <summary>
        /// Actor位置服务客户端
        /// </summary>
        internal static ActorLocationClient LocationClient => ActorProvider.LocationClient;
        /// <summary>
        /// Actor服务提供类
        /// </summary>
        internal static ActorProvider ActorProvider { get; } = new ActorProvider();

        /// <summary>
        /// 创建Actor提供服务
        /// </summary>
        /// <param name="locationIpAddress"></param>
        /// <param name="locationPort"></param>
        /// <param name="localIpAddress"></param>
        /// <param name="localPort"></param>
        /// <returns></returns>
        public static ActorProvider CreateProviderService(string locationIpAddress, int locationPort, string localIpAddress, int localPort)
        {
            LocationIpAddress = locationIpAddress;
            LocationPort = locationPort;
            LocalIpAddress = localIpAddress;
            LocalPort = localPort;
            ActorProvider.LocalActorServer = new LocalActorServer(ActorProvider, localIpAddress, localPort);

            return ActorProvider;
        }

        /// <summary>
        /// 注册一个Actor，新建的Actor会被注册到位置服务中，在同一分组中的服务可以调用。
        /// </summary>
        /// <typeparam name="TActor">Actor实现类或者Actor接口</typeparam>
        /// <param name="actorId">Actor Id</param>
        /// <returns></returns>
        public static async Task<TActor> AddActor<TActor>(object actorId = null) where TActor : IActor
        {
            return await ActorProvider.AddActor<TActor>(actorId);
        }

        /// <summary>
        /// 获取一个Actor，在同一分组中的服务可以调用。
        /// </summary>
        /// <typeparam name="TActor">Actor实现类或者Actor接口</typeparam>
        /// <param name="actorId">Actor Id</param>
        /// <returns></returns>
        public static async Task<TActor> GetActor<TActor>(object actorId = null) where TActor : IActor
        {
            return await ActorProvider.GetActor<TActor>(actorId);
        }

        /// <summary>
        /// 获取一个Actor集合，在同一分组中的服务可以调用。
        /// </summary>
        /// <typeparam name="TActor">Actor实现类或者Actor接口</typeparam>
        /// <param name="actorIds">ActorIds</param>
        /// <returns></returns>
        public static async Task<List<TActor>> GetActorList<TActor>(IEnumerable<object> actorIds) where TActor : IActor
        {
            return await ActorProvider.GetActorList<TActor>(actorIds);
        }

        /// <summary>
        /// 删除并取消Actor注册，在Actor被取消注册时，位置服务会告诉所有分布式服务该Actor失效。
        /// </summary>
        /// <param name="actorId"></param>
        public static void RemoveActor<TActor>(object actorId) where TActor : IActor
        {
            ActorProvider.RemoveActor<TActor>(actorId);
        }
    }
}
