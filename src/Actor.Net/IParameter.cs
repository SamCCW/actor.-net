﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Actor.Net
{
    /// <summary>
    /// Actor返回Task泛型方法参数接口
    /// </summary>
    public interface IReturnTaskResultParameter
    {
        object GetTaskCompletionSource();
        object GetTask(object taskCompletionSource);
        void SetTaskResult(object taskCompletionSource, object result);
        void SetException(object taskCompletionSource, Exception exception);
        Type GetReturnType();
        Task<object> StartFuncAsync(IActor actor, MethodInfo method, object parameter);
    }

    /// <summary>
    /// Actor返回void方法参数接口
    /// </summary>
    public interface IReturnVoidParameter
    {
        void StartFunc(IActor actor, MethodInfo method, object parameter);
        Task StartFuncAsync(IActor actor, MethodInfo method, object parameter);
    }

    /// <summary>
    /// Actor方法参数接口
    /// </summary>
    public interface IParameter
    {

    }
}
