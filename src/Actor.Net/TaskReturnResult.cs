﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Actor.Net
{
    public class TaskReturnResult<TReturnValue>
    {
        public object GetTaskCompletionSource()
        {
            return new TaskCompletionSource<TReturnValue>();
        }

        public object GetTask(object taskCompletionSource)
        {
            var tcs = taskCompletionSource as TaskCompletionSource<TReturnValue>;
            return tcs.Task;
        }

        public void SetTaskResult(object taskCompletionSource, object result)
        {
            var tcs = taskCompletionSource as TaskCompletionSource<TReturnValue>;
            tcs.SetResult((TReturnValue)result);
        }

        public void SetException(object taskCompletionSource, Exception exception)
        {
            var tcs = taskCompletionSource as TaskCompletionSource<TReturnValue>;
            tcs.SetException(exception);
        }
    }
}