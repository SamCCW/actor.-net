﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Actor.Net.Network.Actor
{
    /// <summary>
    /// 调用方式
    /// </summary>
    public enum CallType
    {
        /// <summary>
        /// 异步
        /// </summary>
        Async = 1,
        /// <summary>
        /// 同步
        /// </summary>
        Sync = 2,
    }

    /// <summary>
    /// RPC调用上下文接口
    /// </summary>
    public interface IActorCallContext
    {
        /// <summary>
        /// 反序列化结果
        /// </summary>
        /// <param name="transferContext"></param>
        void SetDeserializationResult(ActorTransferContext transferContext);

        /// <summary>
        /// 通讯管道
        /// </summary>
        ActorBaseChannel Channel { get; }

        /// <summary>
        /// Rpc Id
        /// </summary>
        int RpcId { get; set; }

        /// <summary>
        /// 清空当前上下文参数
        /// </summary>
        void ClearContext();

        /// <summary>
        /// 设置默认结果
        /// </summary>
        /// <returns></returns>
        void SetDefaultResult();

        /// <summary>
        /// 获取Task对象
        /// </summary>
        /// <returns></returns>
        object GetTask();
    }
}
