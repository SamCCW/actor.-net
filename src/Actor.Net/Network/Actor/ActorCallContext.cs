﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Actor.Net.Network.Actor
{
    public class ActorCallContext<TResult> : IActorCallContext
        where TResult : new()
    {
        /// <summary>
        /// 通讯管道
        /// </summary>
        public ActorBaseChannel Channel { get; set; }

        /// <summary>
        /// RPC调用Id
        /// </summary>
        public int RpcId { get; set; }

        /// <summary>
        /// RPC同步信号
        /// </summary>
        private TaskCompletionSource<TResult> tcs;

        /// <summary>
        /// 指定返回类型集合
        /// </summary>
        private Type[] responseTypes;

        /// <summary>
        /// 处理结果
        /// </summary>
        public TResult result;


        /// <summary>
        /// 设置当前上下文参数
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rpcId"></param>
        /// <param name="tcs"></param>
        public void SetContext(ActorBaseChannel channel, int rpcId, TaskCompletionSource<TResult> tcs)
        {
            this.Channel = channel;
            this.RpcId = rpcId;
            this.tcs = tcs;
            this.result = default;
        }

        /// <summary>
        /// 设置当前上下文参数
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rpcId"></param>
        /// <param name="tcs"></param>
        /// <param name="responseTypes"></param>
        public void SetContext(ActorBaseChannel channel, int rpcId, TaskCompletionSource<TResult> tcs, Type[] responseTypes)
        {
            this.Channel = channel;
            this.RpcId = rpcId;
            this.tcs = tcs;
            this.result = default;
            this.responseTypes = responseTypes;
        }

        /// <summary>
        /// 清空当前上下文参数
        /// </summary>
        public void ClearContext()
        {
            this.Channel = null;
            this.RpcId = 0;
            this.tcs = null;
            this.result = default;
            this.responseTypes = null;
        }

        /// <summary>
        /// 反序列化结果
        /// </summary>
        /// <param name="transferContext"></param>
        public void SetDeserializationResult(ActorTransferContext transferContext)
        {
            try
            {
                var type = typeof(TResult);
                if (type == typeof(byte[]))
                {
                    var len = (int)transferContext.BodyStream.Length;
                    object bytes = new byte[len];
                    transferContext.BodyStream.Read(bytes as byte[], 0, len);
                    result = (TResult)bytes;
                }
                else if (type == typeof(string))
                {
                    var len = (int)transferContext.BodyStream.Length;
                    var bytes = new byte[transferContext.BodyStream.Length];
                    transferContext.BodyStream.Read(bytes, 0, bytes.Length);
                    object str = Encoding.UTF8.GetString(bytes);
                    result = (TResult)str;
                }
                else
                {
                    if(this.responseTypes == null)
                    {
                        this.result = MessagePack.MessagePackSerializer.Deserialize<TResult>(transferContext.BodyStream);
                    }
                    else
                    {
                        object objectResult;
                        var resultList = new List<object>();
                        for(var i=0; i< this.responseTypes.Length; i++)
                        {
                            var responseType = this.responseTypes[i];
                            var value = MessagePack.MessagePackSerializer.Deserialize(responseType, transferContext.BodyStream);
                            resultList.Add(value);
                        }
                        objectResult = resultList;
                        this.result = (TResult)objectResult;
                    }
                }
            }
            catch (Exception ex)
            {
                transferContext.ChannelContext.Channel.NetService.ProcessSocketError(transferContext.ChannelContext, ex);
            }
            finally
            {
                if (tcs != null)
                {
                    tcs.TrySetResult(result);
                }
            }
        }

        /// <summary>
        /// 设置默认结果
        /// </summary>
        /// <returns></returns>
        public void SetDefaultResult()
        {
            this.result = default;
            tcs?.TrySetResult(result);
        }

        /// <summary>
        /// 获取Task对象
        /// </summary>
        /// <returns></returns>
        public object GetTask()
        {
            return this.tcs.Task;
        }
    }
}
