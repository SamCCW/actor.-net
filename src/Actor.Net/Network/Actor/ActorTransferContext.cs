﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Actor.Net.Network.Actor
{
    /// <summary>
    /// 传输上下文
    /// </summary>
    public struct ActorTransferContext
    {
        /// <summary>
        /// 标志位
        /// </summary>
        public byte BitFlag { get; set; }
        /// <summary>
        /// 网络指令
        /// </summary>
        public int Command { get; set; }
        /// <summary>
        /// RpcId
        /// </summary>
        public int RpcId { get; set; }
        /// <summary>
        /// 发送者类型
        /// </summary>
        public ActorSenderType Sender { get; set; }
        /// <summary>
        /// 接收字节流
        /// </summary>
        public MemoryStream BodyStream { get; set; }
        /// <summary>
        /// 管道Id
        /// </summary>
        public int ChannelId => this.ChannelContext.ChannelId;
        /// <summary>
        /// 管道上下文
        /// </summary>
        public ActorChannelContext ChannelContext { get; internal set; }
        /// <summary>
        /// 拥有此管道连接业务拥有者Id，比如PlayerId代表该连接属于某个玩家的长链接。
        /// </summary>
        public long UserId => this.ChannelContext.UserId;

        /// <summary>
        /// 获取BodyStream的对象
        /// </summary>
        /// <returns></returns>
        public object GetStream() => this.BodyStream;

        /// <summary>
        /// 输出
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="message"></param>
        /// <param name="timeoutMillisecond"></param>
        public void Output<TMessage>(TMessage message, int timeoutMillisecond = 1000 * 120)
        {
            this.ChannelContext.Response(message, this.Command, this.RpcId, timeoutMillisecond);
        }
    }
}