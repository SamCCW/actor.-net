﻿

namespace Actor.Net.Network.Actor
{
    /// <summary>
    /// 消息发送者类型，区分消息是否是RPC消息，协议设定RPC消息是支持双向的，需要标识出RPC消息是否为调用消息与回馈消息
    /// </summary>
    public enum ActorSenderType : byte
    {
        /// <summary>
        /// 未定义类型
        /// </summary>
        None = 0,
        /// <summary>
        /// 普通消息发送者
        /// </summary>
        NormalSender = 1,
        /// <summary>
        /// RPC调用消息发送者
        /// </summary>
        CallSender = 2,
        /// <summary>
        /// RPC返回消息发送者
        /// </summary>
        ResponseSender = 3,
        /// <summary>
        /// KCP连接报文发送者
        /// </summary>
        KcpConnectionMessage = 4,
    }
}
