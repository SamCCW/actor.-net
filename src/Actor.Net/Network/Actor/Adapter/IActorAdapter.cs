﻿

using System.Threading.Tasks;

namespace Actor.Net.Network.Actor.Adapter
{
    /// <summary>
    /// 消息适配器接口
    /// </summary>
    /// <typeparam name="TRequestMessage"></typeparam>
    public interface IActorAdapter<in TRequestMessage> : IActorAdapter
    {
        /// <summary>
        /// 网络IO接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        void OnRequest(ActorTransferContext context, TRequestMessage requestMessage);
    }

    /// <summary>
    /// 消息适配器接口
    /// </summary>
    public interface IActorAdapter
    {
        /// <summary>
        /// 消息分发接口
        /// </summary>
        /// <param name="context"></param>
        void DispatchAdapter(ActorTransferContext context);
    }
}