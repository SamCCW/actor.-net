﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Actor.Net.Network.Actor
{
    /// <summary>
    /// 通讯通道Id生成器
    /// </summary>
    public static class ActorChannelIdCreator
    {
        private const int MinSize = 0x1000000;
        private const int MaxSize = 0xFFFFFFF;
        private static int incId = MinSize;
        public static int CreateId()
        {
            Interlocked.CompareExchange(ref incId, MinSize, MaxSize);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }

    /// <summary>
    /// Network Id生成器
    /// </summary>
    public static class ActorNetworkIdCreator
    {
        private const int MinSize = 0x1000000;
        private const int MaxSize = 0xFFFFFFF;
        private static int incId = MinSize;
        public static int CreateId()
        {
            Interlocked.CompareExchange(ref incId, MinSize, MaxSize);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }
}
