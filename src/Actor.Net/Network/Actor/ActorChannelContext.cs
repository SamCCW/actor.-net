﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Actor.Net.Network.Actor
{
    /// <summary>
    /// 通讯管道上下文
    /// </summary>
    public class ActorChannelContext
    {
        /// <summary>
        /// 通讯管道
        /// </summary>
        public ActorBaseChannel Channel { get; }
        /// <summary>
        /// 拥有此管道连接业务拥有者Id，比如PlayerId代表该连接属于某个玩家的长链接。
        /// </summary>
        public long UserId => this.Channel.UserId;
        /// <summary>
        /// Network Id
        /// </summary>
        public int NetworkId => this.Channel.NetworkId;
        /// <summary>
        /// 通讯管道Id
        /// </summary>
        public int ChannelId => Channel.ChannelId;
        /// <summary>
        /// 连接状态
        /// </summary>
        public bool Connected => Channel.Connected;
        /// <summary>
        /// 发送缓冲区字节流
        /// </summary>
        public MemoryStream SendStream { get; } = new MemoryStream(8192);
        /// <summary>
        /// 发送线程锁
        /// </summary>
        public object SendLocker { get; } = new object();
        /// <summary>
        /// Actor服务HostKey
        /// </summary>
        internal string ActorHostKey { get; set; }


        private ConcurrentDictionary<Type, ConcurrentQueue<IActorCallContext>> callContextCache = new ConcurrentDictionary<Type, ConcurrentQueue<IActorCallContext>>();

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="channel">通讯管道</param>
        public ActorChannelContext(ActorBaseChannel channel)
        {
            this.Channel = channel;
        }

        /// <summary>
        /// 发送消息
        /// </summary>;
        /// <exception cref="SocketException">Socket异常</exception>
        /// <param name="message">发送内容</param>
        /// <param name="command">消息指令</param>
        /// <param name="timeoutMillisecond">超时时间</param>
        /// <returns>返回发送状态</returns>
        public void Send<TMessage>(TMessage message, int command, int timeoutMillisecond = 1000 * 120)
        {
            this.Send(message, command, 0, ActorSenderType.NormalSender, timeoutMillisecond);
        }

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="sendStream"></param>
        /// <param name="command"></param>
        internal void SendActorMessage(MemoryStream sendStream, int command, int rpcId, ActorSenderType senderType)
        {            
            var dataSize = (int)sendStream.Length;
            sendStream.Seek(0, SeekOrigin.Begin);

            this.WriteHeadBytes(sendStream, 0, 0, command, rpcId, senderType);
            this.WriteLength(sendStream, dataSize);

            this.Channel.Write(sendStream.GetBuffer(), dataSize);
        }

        /// <summary>
        /// 发送Actor调用信息
        /// </summary>
        /// <param name="sendStream"></param>
        /// <param name="command"></param>
        /// <param name="rpcId"></param>
        /// <param name="resultTypes"></param>
        /// <param name="senderType"></param>
        /// <param name="cancellationToken"></param>
        public void SendCallActorMessage(MemoryStream sendStream, int command, int rpcId, Type[] resultTypes, ActorSenderType senderType, CancellationToken cancellationToken = default)
        {
            this.SendActorMessage(sendStream, command, rpcId, senderType);

            ActorCallContext<List<object>> callContext = this.DequeueCallContext<List<object>>();
            var tcs = new TaskCompletionSource<List<object>>();
            callContext.SetContext(this.Channel, rpcId, tcs, resultTypes);
            this.Channel.CallContextList.TryAdd(rpcId, callContext);
            cancellationToken.Register(() =>
            {
                if (!tcs.Task.IsCompleted)
                    tcs.TrySetResult(default);
            });
        }

        /// <summary>
        /// 获取Actor调用结果
        /// </summary>
        /// <param name="command"></param>
        /// <param name="rpcId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<object>> GetActorCallResultAsync(int command, int rpcId,  CancellationToken cancellationToken = default)
        {
            if (!this.Channel.CallContextList.TryGetValue(rpcId, out IActorCallContext callContext))
                return default;

            try
            {
                var task = callContext.GetTask() as Task<List<object>>;
                return await task;
            }
            finally
            {
                this.EnqueueCallContext(callContext);
            }
        }


        /// <summary>
        /// 请求消息
        /// </summary>
        /// <exception cref="SocketException">Socket异常</exception>
        /// <typeparam name="TResult">返回结果类型</typeparam>
        /// <param name="message">发送消息</param>
        /// <param name="command">消息指令</param>
        /// <param name="cancellationToken">取消令牌</param>
        /// <returns>返回消息结果</returns>
        public async Task<TResult> CallAsync<TResult>(object message, int command, CancellationToken cancellationToken = default)
            where TResult : new()
        {
            ActorCallContext<TResult> callContext = this.DequeueCallContext<TResult>();
            try
            {
                int rpcId = this.CreateIncId();
                this.Send(message, command, rpcId, ActorSenderType.CallSender);

                var tcs = new TaskCompletionSource<TResult>();
                callContext.SetContext(this.Channel, rpcId, tcs);
                this.Channel.CallContextList.TryAdd(rpcId, callContext);
                cancellationToken.Register(() =>
                {
                    if (!tcs.Task.IsCompleted)
                        tcs.TrySetResult(default);
                });
                return await tcs.Task;
            }
            finally
            {
                this.EnqueueCallContext(callContext);
            }
        }

        /// <summary>
        /// 发送应答消息
        /// </summary>
        /// <exception cref="SocketException">Socket异常</exception>
        /// <param name="message">发送消息内容</param>
        /// <param name="command">消息指令</param>
        /// <param name="rpcId">Rpc调用Id</param>
        /// <param name="timeoutMillisecond">超时时间</param>
        /// <returns>返回发送状态</returns>
        internal void Response<TMessage>(TMessage message, int command, int rpcId, int timeoutMillisecond = 1000 * 120)
        {
            this.Send(message, command, rpcId, ActorSenderType.ResponseSender, timeoutMillisecond);
        }

        private void Send(object message, int command, int rpcId, ActorSenderType sender, int timeoutMillisecond = 1000 * 120)
        {
            var sendStream = this.SendStream;
            lock (SendLocker)
            {
                try
                {
                    this.WriteHeadBytes(sendStream, 0, 0, command, rpcId, sender);
                    if (message != null)
                    {
                        var type = message.GetType();
                        if (type == typeof(string))
                        {
                            var str = message as string;
                            var bytes = Encoding.UTF8.GetBytes(str);
                            sendStream.Write(bytes, 0, bytes.Length);
                        }
                        else if (type == typeof(byte[]))
                        {
                            var bytes = message as byte[];
                            sendStream.Write(bytes, 0, bytes.Length);
                        }
                        else
                        {
                            MessagePack.MessagePackSerializer.Serialize(sendStream, message);
                        }
                    }
                    int length = (int)sendStream.Position;
                    this.WriteLength(sendStream, length);

                    var sendBuffer = sendStream.GetBuffer();
                    this.Channel.Write(sendBuffer, length);
                }
                finally
                {
                    sendStream.Seek(0, SeekOrigin.Begin);
                    sendStream.SetLength(0);
                }
            }
        }

        private ActorCallContext<TResult> DequeueCallContext<TResult>()
            where TResult : new()
        {
            var type = typeof(ActorCallContext<TResult>);
            if(!this.callContextCache.TryGetValue(type, out ConcurrentQueue<IActorCallContext> queue))
            {
                queue = new ConcurrentQueue<IActorCallContext>();
                this.callContextCache.AddOrUpdate(type, queue, (k, v) => queue);
            }

            if(!queue.TryDequeue(out IActorCallContext context))
            {
                context = new ActorCallContext<TResult>();
            }

            return context as ActorCallContext<TResult>;
        }

        private void EnqueueCallContext(IActorCallContext context)
        {
            var type = context.GetType();
            if (!this.callContextCache.TryGetValue(type, out ConcurrentQueue<IActorCallContext> queue))
            {
                queue = new ConcurrentQueue<IActorCallContext>();
                this.callContextCache.AddOrUpdate(type, queue, (k, v) => queue);
            }

            context.ClearContext();
            queue.Enqueue(context);
        }

        private void WriteHeadBytes(MemoryStream stream, int length, byte bitFlag, int command, int rpcId, ActorSenderType sender)
        {
            this.WriteTo(stream, length);
            stream.WriteByte(bitFlag);
            this.WriteTo(stream, command);
            this.WriteTo(stream, rpcId);
            stream.WriteByte((byte)sender);
        }

        private void WriteLength(MemoryStream stream, int length)
        {
            const int count = sizeof(int);
            for (var i = 0; i < count; i++)
            {
                var d = (byte)(length >> i * 8);
                stream.GetBuffer()[i] = d;
            }
        }

        private void WriteTo(MemoryStream stream, int value)
        {
            const int count = sizeof(int);
            for (var i = 0; i < count; i++)
            {
                var d = (byte)(value >> i * 8);
                stream.WriteByte(d);
            }
        }

        private int incId;
        internal int CreateIncId()
        {
            Interlocked.CompareExchange(ref this.incId, 0, int.MaxValue);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }
}
