﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Actor.Net.Network.Actor
{
    public enum ParseState : byte
    {
        OnLength = 0,
        OnBitFlag = 1,
        OnCommand = 2,
        OnRpcId = 3,
        OnSender = 4,
        OnBody = 5,
    }

    //-----------------------------Socket包头-------------------------
    //  |----------------|----|----------------|----------------|----|
    //      Length        bit     command           rpc id      sender
    //      4byte        1byte     4byte             4byte      1byte
    //----------------------------------------------------------------
    //  Length  包长度
    //  bit     标志位,KCP握手SYN,ACK标识，数据包加密压缩标识（目前版本没有使用到）
    //  command 消息总线CMD
    //  rpc id  rpc消息唯一标识
    //  sender  发送端标识，参考SenderType枚举，实现双向管道通讯的协议

    public class ActorPacketParser
    {
        public const int LengthSize = 0x4;
        public const int BitFlagSize = 0x1;
        public const int CommandSize = 0x4;
        public const int RpcIdSize = 0x4;
        public const int SenderSize = 0x1;
        public const int HeadSize = LengthSize + BitFlagSize + CommandSize + RpcIdSize + SenderSize;

        public const int LengthOffset = 0;
        public const int BitFlagOffset = LengthOffset + LengthSize;
        public const int CommandOffset = BitFlagOffset + BitFlagSize;
        public const int RpcIdOffset = CommandOffset + CommandSize;
        public const int SenderOffset = RpcIdOffset + RpcIdSize;

        public const int BufferLength = 8192;

        public int BodySize => this.length - HeadSize;
        public MemoryStream BodyBuffer { get; } = new MemoryStream(BufferLength);
        public MemoryStream HeadBuffer { get; } = new MemoryStream(HeadSize);

        private int length;
        private byte bitFlag;
        private int command;
        private int rpcId;
        private ActorSenderType sender;
        private bool isOk;
        private ParseState state;

        public bool Parse(byte[] bytes, ref int offset, ref int count)
        {
            bool finish = false;
            while (true)
            {
                if (finish)
                    break;

                switch (this.state)
                {
                    case ParseState.OnLength:
                        if(this.ParseLength(bytes, ref offset, ref count))
                        {
                            this.length = BitConverter.ToInt32(HeadBuffer.GetBuffer(), 0);
                            this.state = ParseState.OnBitFlag;
                            //非法攻击小包
                            if(this.length < HeadSize)
                            {
                                throw new SocketException((int)SocketError.SocketError);
                            }
                        }
                        break;
                    case ParseState.OnBitFlag:
                        if(ParseBitFlag(bytes, ref offset, ref count))
                        {
                            this.bitFlag = HeadBuffer.GetBuffer()[BitFlagOffset];
                            this.state = ParseState.OnCommand;
                        }
                        break;
                    case ParseState.OnCommand:
                        if(ParseCommand(bytes, ref offset, ref count))
                        {
                            this.command = BitConverter.ToInt32(HeadBuffer.GetBuffer(), CommandOffset);
                            this.state = ParseState.OnRpcId;
                        }
                        break;
                    case ParseState.OnRpcId:
                        if(ParseRpcId(bytes, ref offset, ref count))
                        {
                            this.rpcId = BitConverter.ToInt32(HeadBuffer.GetBuffer(), RpcIdOffset);
                            this.state = ParseState.OnSender;
                        }
                        break;
                    case ParseState.OnSender:
                        if(ParseSender(bytes, ref offset, ref count))
                        {
                            this.sender = (ActorSenderType)HeadBuffer.GetBuffer()[SenderOffset];
                            this.state = ParseState.OnBody;
                            if(this.BodySize == 0)
                            {
                                isOk = true;
                                this.state = ParseState.OnLength;
                            }

                        }
                        break;
                    case ParseState.OnBody:
                        if(ParseBody(bytes, ref offset, ref count))
                        {
                            isOk = true;
                            this.state = ParseState.OnLength;
                        }
                        break;
                }

                if (isOk)
                {
                    this.BodyBuffer.Seek(0, SeekOrigin.Begin);
                    this.BodyBuffer.SetLength(this.BodySize);

                    finish = true;
                }
                else if (count == 0)
                {
                    finish = true;
                }
            }
            return isOk;
        }

        public void Flush()
        {
            this.isOk = false;
            this.length = 0;
            this.bitFlag = 0;
            this.command = 0;
            this.rpcId = 0;
            this.sender = ActorSenderType.None;
            this.state = ParseState.OnLength;

            this.HeadBuffer.Seek(0, SeekOrigin.Begin);
            this.HeadBuffer.SetLength(0);

            this.BodyBuffer.Seek(0, SeekOrigin.Begin);
            this.BodyBuffer.SetLength(0);
        }

        public ActorTransferContext GetTransferContext(ActorChannelContext channelContext)
        {
            return new ActorTransferContext
            {
                BitFlag = this.bitFlag,
                Command = this.command,
                RpcId = this.rpcId,
                Sender = this.sender,
                BodyStream = this.BodyBuffer,
                ChannelContext = channelContext,
            };
        }

        private bool ParseLength(byte[] bytes, ref int offset, ref int count)
        {
            var needSize = LengthSize - (int)HeadBuffer.Length;
            if (count >= needSize)
            {
                HeadBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                HeadBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)HeadBuffer.Length == BitFlagOffset;
        }

        private bool ParseBitFlag(byte[] bytes, ref int offset, ref int count)
        {
            var needSize = LengthSize + BitFlagSize - (int)HeadBuffer.Length;
            if (count >= needSize)
            {
                HeadBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                HeadBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)HeadBuffer.Length == CommandOffset;
        }

        private bool ParseCommand(byte[] bytes, ref int offset, ref int count)
        {
            var needSize = LengthSize + BitFlagSize + CommandSize - (int)HeadBuffer.Length;
            if (count >= needSize)
            {
                HeadBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                HeadBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)HeadBuffer.Length == RpcIdOffset;
        }

        private bool ParseRpcId(byte[] bytes, ref int offset, ref int count)
        {
            var needSize = LengthSize + BitFlagSize + CommandSize + RpcIdSize - (int)HeadBuffer.Length;
            if (count >= needSize)
            {
                HeadBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                HeadBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)HeadBuffer.Length == SenderOffset;
        }

        private bool ParseSender(byte[] bytes, ref int offset, ref int count)
        {
            var needSize = LengthSize + BitFlagSize + CommandSize + RpcIdSize + SenderSize - (int)HeadBuffer.Length;
            if (count >= needSize)
            {
                HeadBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                HeadBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)HeadBuffer.Length == SenderOffset + SenderSize;
        }

        private bool ParseBody(byte[] bytes, ref int offset, ref int count)
        {
            var bodySize = this.BodySize;
            var needSize = bodySize - (int)BodyBuffer.Length;
            if (count >= needSize)
            {
                BodyBuffer.Write(bytes, offset, needSize);
                offset += needSize;
                count -= needSize;
            }
            else
            {
                BodyBuffer.Write(bytes, offset, count);
                offset += count;
                count -= count;
            }
            return (int)BodyBuffer.Length == bodySize;
        }
    }
}
