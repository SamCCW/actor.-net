﻿using System;

namespace Actor.Net.Network.Actor
{
    public static class ActorTimeUtils
    {
        private static readonly long Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

        /// <summary>
        /// 毫秒
        /// </summary>
        /// <returns></returns>
        public static long NowMilliSecond()
        {
            return (DateTime.UtcNow.Ticks - Epoch) / 10000;
        }

        /// <summary>
        /// 微秒
        /// </summary>
        /// <returns></returns>
        public static long NowMicrosecond()
        {
            return (DateTime.UtcNow.Ticks - Epoch) / 10;
        }
    }
}
