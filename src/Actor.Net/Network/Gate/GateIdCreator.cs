﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Actor.Net.Network.Gate
{
    /// <summary>
    /// 通讯通道Id生成器
    /// </summary>
    public static class GateChannelIdCreator
    {
        private const int MinSize = 0x1000000;
        private const int MaxSize = 0xFFFFFFF;
        private static int incId = MinSize;
        public static int CreateId()
        {
            Interlocked.CompareExchange(ref incId, MinSize, MaxSize);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }

    /// <summary>
    /// Network Id生成器
    /// </summary>
    public static class GateNetworkIdCreator
    {
        private const int MinSize = 0x1000000;
        private const int MaxSize = 0xFFFFFFF;
        private static int incId = MinSize;
        public static int CreateId()
        {
            Interlocked.CompareExchange(ref incId, MinSize, MaxSize);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }

    /// <summary>
    /// KCP连接确认号Conv生成器
    /// </summary>
    public static class KcpConvIdCreator
    {
        private const int MinSize = 100000;
        private const int MaxSize = 0xFFFFFF;
        private static int incId = MinSize;
        public static int CreateId()
        {
            Interlocked.CompareExchange(ref incId, MinSize, MaxSize);
            Interlocked.Increment(ref incId);
            return incId;
        }
    }
}
