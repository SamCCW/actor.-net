﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Actor.Net.Network.Gate
{
    /// <summary>
    /// RPC调用上下文接口
    /// </summary>
    public interface IGateCallContext
    {
        /// <summary>
        /// 反序列化结果
        /// </summary>
        /// <param name="transferContext"></param>
        void SetDeserializationResult(GateTransferContext transferContext);

        /// <summary>
        /// 通讯管道
        /// </summary>
        GateBaseChannel Channel { get; }

        /// <summary>
        /// Rpc Id
        /// </summary>
        int RpcId { get; set; }

        /// <summary>
        /// 清空当前上下文参数
        /// </summary>
        void ClearContext();

        /// <summary>
        /// 设置默认结果
        /// </summary>
        /// <returns></returns>
        void SetDefaultResult();
    }
}
