﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Actor.Net.Network.Gate
{
    public enum NetServiceType
    {
        Undefined = 0,
        Client = 1,
        Server = 2,
    }

    /// <summary>
    /// 网络服务基类
    /// </summary>
    public abstract class GateBaseService : IDisposable
    {
        public GateNetwork Network { get; }
        public GateBaseChannel ConnectChannel { get; set; }
        public NetServiceType ServiceType { get; set; }
        public Action<GateChannelContext> OnConnected { get; set; }
        public Action<GateChannelContext> OnDisconnected { get; set; }
        public Action<GateChannelContext, Exception> OnChannelError { get; set; }
        public Action<GateChannelContext> OnReconnectFailed { get; set; }
        public ConcurrentDictionary<int, GateChannelContext> ConnectingChannels { get; } = new ConcurrentDictionary<int, GateChannelContext>();
        public static ConcurrentDictionary<int, GateChannelContext> ChannelContexts { get; } = new ConcurrentDictionary<int, GateChannelContext>();

        public GateBaseService(GateNetwork network)
        {
            this.Network = network;
        }

        public abstract GateChannelContext Connect(int timeoutMillisecond);
        public abstract Task<GateChannelContext> ConnectAsync(CancellationToken cancellationToken);
        public abstract void Listen();

        protected int CreateChannelId()
        {
            int id = GateChannelIdCreator.CreateId();
            while (true)
            {
                if (GateBaseService.ChannelContexts.ContainsKey(id))
                    id = GateChannelIdCreator.CreateId();
                else
                    break;
            }
            return id;
        }

        public abstract void Update();

        internal virtual void ProcessConnected(GateChannelContext context)
        {
            if (GateBaseService.ChannelContexts.ContainsKey(context.ChannelId))
                return;

            if (!context.Channel.Connected)
                return;

            GateBaseService.ChannelContexts.AddOrUpdate(context.ChannelId, context, (k, v) => context);
            try
            {
                this.OnConnected?.Invoke(context);
            }
            catch(Exception ex)
            {
                this.ProcessSocketError(context, ex);
            }
        }

        internal virtual void ProcessDisconnected(GateChannelContext context)
        {
            if (!GateBaseService.ChannelContexts.TryRemove(context.ChannelId, out context))
                return;

            if (this.ServiceType == NetServiceType.Client)
            {
                try
                {
                    this.OnDisconnected?.Invoke(context);
                }
                catch(Exception ex)
                {
                    this.ProcessSocketError(context, ex);
                }
                return;
            }

            try
            {
                this.OnDisconnected?.Invoke(context);
            }
            catch(Exception ex)
            {
                this.ProcessSocketError(context, ex);
            }
        }

        internal void ProcessSocketError(GateChannelContext context, Exception exception)
        {
            try
            {
                this.OnChannelError?.Invoke(context, exception);
            }
            catch{ }
        }

        public abstract void Dispose();
    }
}
