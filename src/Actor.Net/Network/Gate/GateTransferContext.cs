﻿using Google.Protobuf;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Actor.Net.Network.Gate
{
    /// <summary>
    /// 传输上下文
    /// </summary>
    public struct GateTransferContext
    {
        /// <summary>
        /// 网络指令
        /// </summary>
        public ushort Command { get; set; }
        /// <summary>
        /// RpcId
        /// </summary>
        public int RpcId { get; set; }
        /// <summary>
        /// 接收字节流
        /// </summary>
        public Stream ReceiveBodyStream { get; set; }
        /// <summary>
        /// 管道上下文
        /// </summary>
        public GateChannelContext ChannelContext { get; internal set; }
        /// <summary>
        /// 拥有此管道连接业务拥有者Id，比如PlayerId代表该连接属于某个玩家的长链接。
        /// </summary>
        public long OwnerId => this.ChannelContext.OwnerId;

        /// <summary>
        /// 输出
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="message"></param>
        public void Response<TMessage>(TMessage message)
        {
            this.ChannelContext.Response(message, this.Command, this.RpcId);
        }
    }
}