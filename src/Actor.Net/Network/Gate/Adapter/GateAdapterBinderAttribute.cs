﻿using System;

namespace Actor.Net.Network.Gate.Adapter
{
    /// <summary>
    /// 网络适配器绑定特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class GateAdapterBinderAttribute : Attribute
    {
        /// <summary>
        /// 消息Id
        /// </summary>
        public int Command { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="command"></param>
        public GateAdapterBinderAttribute(object command)
        {
            Command = Convert.ToInt32(command);
        }
    }
}
