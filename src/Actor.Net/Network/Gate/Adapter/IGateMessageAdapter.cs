﻿

using Google.Protobuf;
using System.Threading.Tasks;

namespace Actor.Net.Network.Gate.Adapter
{
    /// <summary>
    /// 消息适配器接口
    /// </summary>
    /// <typeparam name="TRequestMessage"></typeparam>
    public interface IGateMessageAdapter<in TRequestMessage> : IGateMessageAdapter
    {
        /// <summary>
        /// 网络IO接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        void OnRequest(GateTransferContext context, TRequestMessage requestMessage);
    }

    /// <summary>
    /// 消息适配器接口
    /// </summary>
    public interface IGateMessageAdapter
    {
        /// <summary>
        /// 消息分发接口
        /// </summary>
        /// <param name="context"></param>
        void DispatchAdapter(GateTransferContext context);
    }
}