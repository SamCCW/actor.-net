﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Network.Gate
{
    public enum GateNetType
    {
        Tcp = 0,
        WebSocket = 1,
    }
}