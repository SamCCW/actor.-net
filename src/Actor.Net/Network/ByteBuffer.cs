﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Network
{
    public class ByteBuffer
    { 
        public byte[] ByteArray { get; } = new byte[Capacity];

        public int Length;
        public static int Capacity = 512;
    }
}
