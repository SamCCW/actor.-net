﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Location
{
    /// <summary>
    /// 删除一个Actor位置信息网络适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.ACTOR_REMOVE_FLAG)]
    public class RemoveActorLocationAdapter : ActorAdapter<ActorLocationInfo>
    {
        /// <summary>
        /// 网络IO接口实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        public override void OnRequest(ActorTransferContext context, ActorLocationInfo requestMessage)
        {
            ActorProviderService.LocationClient.RemoveContext(requestMessage);
        }
    }
}
