﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using System;

namespace Actor.Net.Location
{
    /// <summary>
    /// Actor位置信息上下文
    /// </summary>
    public class LocationContext
    {
        /// <summary>
        /// Actor服务分组
        /// </summary>
        public string ServerGroup { get; }
        /// <summary>
        /// Actor类型
        /// </summary>
        public string ActorType { get; }
        /// <summary>
        /// Actor Id
        /// </summary>
        public string ActorId { get; }
        /// <summary>
        /// 是否已经连接
        /// </summary>
        public bool Connected => this.ChannelContext.Connected;
        /// <summary>
        /// 通讯管道上下文
        /// </summary>
        public ActorChannelContext ChannelContext { get; }
        /// <summary>
        /// 位置信息
        /// </summary>
        public ActorLocationInfo LocationInfo { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="locationInfo"></param>
        /// <param name="channelContext"></param>
        public LocationContext(ActorLocationInfo locationInfo, ActorChannelContext channelContext)
        {
            this.ServerGroup = locationInfo.ServerGroup;
            this.ActorType = locationInfo.ActorType;
            this.ActorId = locationInfo.ActorId;
            this.ChannelContext = channelContext;
            this.LocationInfo = locationInfo;
        }
    }
}
