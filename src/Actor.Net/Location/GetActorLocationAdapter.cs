﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Location
{
    /// <summary>
    /// 获取Actor位置信息适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.ACTOR_GET_LOCATION_FLAG)]
    public class GetActorLocationAdapter : ActorAdapter<ActorRouterInfo>
    {
        /// <summary>
        /// 网络IO接口实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        public override void OnRequest(ActorTransferContext context, ActorRouterInfo requestMessage)
        {
            var output = ActorLocationServer.Instance.GetActorLocationInfo(requestMessage.ServerGroup, requestMessage.ActorType, requestMessage.ActorId);
            output = output ?? new ActorLocationInfo
            {
                ServerGroup = requestMessage.ServerGroup,
                ActorType = requestMessage.ActorType,
                ActorId = requestMessage.ActorId,
                Stated = ActorLocationState.ActorLocationNotExisted,
            };
            context.Output(output);
        }
    }
}
