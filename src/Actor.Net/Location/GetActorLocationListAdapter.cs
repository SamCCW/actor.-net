﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Location
{
    /// <summary>
    /// 获取Actor位置信息列表适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.ACTOR_GET_LOCATION_LIST_FLAG)]
    public class GetActorLocationListAdapter : ActorAdapter<List<ActorRouterInfo>>
    {
        public override void OnRequest(ActorTransferContext context, List<ActorRouterInfo> requestMessage)
        {
            var locationInfos = new List<ActorLocationInfo>();
            foreach(var routerInfo in requestMessage)
            {
                var locationInfo = ActorLocationServer.Instance.GetActorLocationInfo(routerInfo.ServerGroup, routerInfo.ActorType, routerInfo.ActorId);
                locationInfo = locationInfo ?? new ActorLocationInfo
                {
                    ServerGroup = routerInfo.ServerGroup,
                    ActorType = routerInfo.ActorType,
                    ActorId = routerInfo.ActorId,
                    Stated = ActorLocationState.ActorLocationNotExisted,
                };
                locationInfos.Add(locationInfo);
            }
            context.Output(locationInfos);
        }
    }
}
