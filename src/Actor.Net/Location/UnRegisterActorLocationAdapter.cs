﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Location
{
    /// <summary>
    /// 取消Actor位置信息注册网络适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.ACTOR_UNREGISTER_FLAG)]
    public class UnRegisterActorLocationAdapter : ActorAdapter<ActorLocationInfo>
    {
        /// <summary>
        /// 网络IO接口实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        public override void OnRequest(ActorTransferContext context, ActorLocationInfo requestMessage)
        {
            ActorLocationServer.Instance.Remove(requestMessage, true);
        }
    }
}
