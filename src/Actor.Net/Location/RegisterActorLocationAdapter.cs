﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;
using System;

namespace Actor.Net.Location
{
    /// <summary>
    /// 注册Actror位置信息网络适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.ACTOR_REGISTER_FLAG)]
    public class RegisterActorLocationAdapter : ActorAdapter<ActorLocationInfo>
    {
        /// <summary>
        /// 网络IO接口实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        public override void OnRequest(ActorTransferContext context, ActorLocationInfo requestMessage)
        {
            ActorLocationServer.Instance.AddGroupInfo(context, new GroupInfo
            {
                ServerGroup = requestMessage.ServerGroup,
                IpAddress = requestMessage.IpAddress,
                Port = requestMessage.Port,
            });
            ActorLocationServer.Instance.AddActorLocationInfo(requestMessage);
            context.Output(requestMessage);
        }
    }
}
