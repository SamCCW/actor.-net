﻿using Actor.Net.Message;
using Actor.Net.Network.Actor;
using Actor.Net.Network.Actor.Adapter;

namespace Actor.Net.Location
{
    /// <summary>
    /// 删除Actor服务分组位置信息网络适配器
    /// </summary>
    [ActorAdapterBinder(CommandRetention.GROUPS_REMOVE_FLAG)]
    public class RemoveGroupLocationAdapter : ActorAdapter<GroupInfo>
    {
        /// <summary>
        /// 网络IO接口实现
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requestMessage"></param>
        public override void OnRequest(ActorTransferContext context, GroupInfo requestMessage)
        {
            ActorProviderService.LocationClient.RemoveActorContextByGroup(requestMessage);
        }
    }
}
