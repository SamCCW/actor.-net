﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Binder
{
    /// <summary>
    /// Actor类型信息
    /// </summary>
    public class ActorTypeInfo
    {
        /// <summary>
        /// Actor服务分组
        /// </summary>
        public string ServerGroup { get; set; }
        /// <summary>
        /// Actor类型
        /// </summary>
        public string ActorType { get; set; }
    }
}
