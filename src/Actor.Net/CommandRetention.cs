﻿
namespace Actor.Net
{
    /// <summary>
    /// 系统保留Command
    /// </summary>
    public class CommandRetention
    {
        /// <summary>
        /// 获取Actor位置信息列表标志
        /// </summary>
        public const int ACTOR_GET_LOCATION_LIST_FLAG = -1000;

        /// <summary>
        /// PING标志
        /// </summary>
        public const int PING_RPC_FLAG = -999;

        /// <summary>
        /// Actor注册标志
        /// </summary>
        public const int ACTOR_REGISTER_FLAG = -998;

        /// <summary>
        /// 获取Actor位置信息标志
        /// </summary>
        public const int ACTOR_GET_LOCATION_FLAG = -997;

        /// <summary>
        /// 删除Actor标志
        /// </summary>
        public const int ACTOR_REMOVE_FLAG = -996;

        /// <summary>
        /// Actor消息标志
        /// </summary>
        public const int ACTOR_MESSAGE_FLAG = -995;

        /// <summary>
        /// 取消ACTOR注册
        /// </summary>
        public const int ACTOR_UNREGISTER_FLAG = -992;

        /// <summary>
        /// 服务分组删除
        /// </summary>
        public const int GROUPS_REMOVE_FLAG = -991;
    }
}
