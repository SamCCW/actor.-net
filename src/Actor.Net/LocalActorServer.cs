﻿using Actor.Net.Location;
using Actor.Net.Network.Actor;
using System.Threading.Tasks;

namespace Actor.Net
{
    /// <summary>
    /// 本地Actor服务
    /// </summary>
    public class LocalActorServer
    {
        /// <summary>
        /// 本地服务网络通讯服务
        /// </summary>
        public ActorNetwork ActorNetworkServer { get; set; }
        /// <summary>
        /// Actor提供实体
        /// </summary>
        private ActorProvider ActorProvider { get; }
        /// <summary>
        /// 本地服务IP地址
        /// </summary>
        public string IpAddress { get; }
        /// <summary>
        /// 本地服务监听端口
        /// </summary>
        public int Port { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="actorProvider">Actor提供实体</param>
        /// <param name="ipAddress">本地服务IP地址</param>
        /// <param name="port">本地服务监听端口</param>
        public LocalActorServer(ActorProvider actorProvider, string ipAddress, int port)
        {
            ActorProvider = actorProvider;
            IpAddress = ipAddress;
            Port = port;
        }

        /// <summary>
        /// 开始监听
        /// </summary>
        public void Listen()
        {
            ActorNetworkServer = new ActorNetwork(IpAddress, Port);
            //ActorNetworkServer.IsSynchronizationContext = false;
            ActorNetworkServer.Bind<GetActorLocationListAdapter>(CommandRetention.ACTOR_GET_LOCATION_LIST_FLAG);
            ActorNetworkServer.Bind<GetActorLocationAdapter>(CommandRetention.ACTOR_GET_LOCATION_FLAG);
            ActorNetworkServer.Bind<RegisterActorLocationAdapter>(CommandRetention.ACTOR_REGISTER_FLAG);
            ActorNetworkServer.Bind<RemoveActorLocationAdapter>(CommandRetention.ACTOR_REMOVE_FLAG);
            ActorNetworkServer.Bind<RemoveGroupLocationAdapter>(CommandRetention.GROUPS_REMOVE_FLAG);
            ActorNetworkServer.Bind<UnRegisterActorLocationAdapter>(CommandRetention.ACTOR_UNREGISTER_FLAG);
            ActorNetworkServer.Bind<UnRegisterActorLocationAdapter>(CommandRetention.ACTOR_UNREGISTER_FLAG);
            ActorNetworkServer.Bind<ActorMessageAdapter>(CommandRetention.ACTOR_MESSAGE_FLAG);
            ActorNetworkServer.IsIsDropTimeout = false;
            ActorNetworkServer.Listen();
        }
    }
}
