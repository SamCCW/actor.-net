﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net
{
    /// <summary>
    /// Actor服务分组特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ActorGroupAttribute : Attribute
    {
        /// <summary>
        /// Actor服务分组
        /// </summary>
        public string ServerGroup { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="serverGroup"></param>
        public ActorGroupAttribute(string serverGroup)
        {
            this.ServerGroup = serverGroup;
        }
    }
}
