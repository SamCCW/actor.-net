﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Actor.Net
{
    public static class DelegateBuilder<T, TReturnValue>
    {
        public static Func<T, Task<TReturnValue>> CreateInstanceTaskResultMethod(IActor actor, MethodInfo method)
        {
            return (Func<T, Task<TReturnValue>>)method.CreateDelegate(typeof(Func<T, Task<TReturnValue>>), actor);
        }
    }

    public static class DelegateBuilder<T>
    {
        public static Func<T, Task> CreateInstanceTaskMethod(IActor actor, MethodInfo method)
        {
            return (Func<T, Task>)method.CreateDelegate(typeof(Func<T, Task>), actor);
        }

        public static Action<T> CreateInstanceActionMethod(IActor actor, MethodInfo method)
        {
            return (Action<T>)method.CreateDelegate(typeof(Action<T>), actor);
        }
    }
}
