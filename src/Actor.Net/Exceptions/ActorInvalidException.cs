﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Exceptions
{
    public class ActorInvalidException : Exception
    {
        public ActorInvalidException(string message) : base(message) { }
    }
}
