﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Exceptions
{
    public class ActorServerException : Exception
    {
        public ActorServerException(string message) : base(message) { }
    }
}
