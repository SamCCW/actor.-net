﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Exceptions
{
    public class ActorRouterNotBindException : Exception
    {
        public ActorRouterNotBindException(string message) : base(message) { }
    }
}
