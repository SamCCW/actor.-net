﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Exceptions
{
    public class ActorUnkonwException : Exception
    {
        public ActorUnkonwException(string message) : base(message) { }
    }
}
