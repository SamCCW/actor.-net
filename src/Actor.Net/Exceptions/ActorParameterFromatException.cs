﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Exceptions
{
    public class ActorParameterFromatException : Exception
    {
        public ActorParameterFromatException(string message) : base(message) { }
    }
}
