﻿using Actor.Net.Location;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net
{
    /// <summary>
    /// Actor抽象类
    /// </summary>
    public abstract class ActorBase : IActor
    {
        /// <summary>
        /// 服务分组
        /// </summary>
        public string ServerGroup => LocationContext.ServerGroup;
        /// <summary>
        /// Actor类型
        /// </summary>
        public string ActorType => LocationContext.ActorType;
        /// <summary>
        /// ActorId
        /// </summary>
        public string ActorId => LocationContext.ActorId;
        /// <summary>
        /// 是否是本地服务
        /// </summary>
        public bool IsLocal { get; set; }
        /// <summary>
        /// 位置信息上下文
        /// </summary>
        public LocationContext LocationContext { get; set; }
        /// <summary>
        /// AOP代理信息
        /// </summary>
        public ActorProxyInfo ProxyInfo { get; set; }

        /// <summary>
        /// 当你需要在你的Actor中干些额外的事情，可以重写Start方法来实现。
        /// Start方法会在Actor被注册了以后执行，例如你调用AddActor来注册一个Actor。
        /// </summary>
        public virtual void Start(){}
    }
}
