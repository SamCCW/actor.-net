﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// 获取Actor位置信息输入参数
    /// </summary>
    [MessagePackObject]
    public class ActorRouterInfo
    {
        /// <summary>
        /// Actor服务分组
        /// </summary>
        [Key(0)]
        public string ServerGroup { get; set; }
        /// <summary>
        /// Actor类型
        /// </summary>
        [Key(1)]
        public string ActorType { get; set; }
        /// <summary>
        /// Actor Id
        /// </summary>
        [Key(2)]
        public string ActorId { get; set; }
    }
}
