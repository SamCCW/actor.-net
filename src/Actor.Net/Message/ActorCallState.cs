﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// Actor调用结果状态
    /// </summary>
    public enum ActorCallState
    {
        /// <summary>
        /// 成功
        /// </summary>
        Successful = 0,
        /// <summary>
        /// 远程Actor服务异常
        /// </summary>
        ActorServerException,
        /// <summary>
        /// 远程Actor服务不存在
        /// </summary>
        ActorNotExisted,
        /// <summary>
        /// 远程服务参数序列化错误
        /// </summary>
        ParameterFromatError,
        /// <summary>
        /// 远程服务Actor路由没有绑定
        /// </summary>
        RouterNotBind,
    }
}
