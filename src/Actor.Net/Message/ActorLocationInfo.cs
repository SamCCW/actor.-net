﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// 本地服务信息
    /// </summary>
    [MessagePackObject]
    public class ActorLocationInfo
    {
        /// <summary>
        /// Actor服务组
        /// </summary>
        [Key(0)]
        public string ServerGroup { get; set; }
        /// <summary>
        /// Actor类型
        /// </summary>
        [Key(1)]
        public string ActorType { get; set; }
        /// <summary>
        /// Actor Id
        /// </summary>
        [Key(2)]
        public string ActorId { get; set; }
        /// <summary>
        /// Actor服务IP地址
        /// </summary>
        [Key(3)]
        public string IpAddress { get; set; }
        /// <summary>
        /// Actor服务监听端口号
        /// </summary>
        [Key(4)]
        public int Port { get; set; }
        /// <summary>
        /// 服务状态
        /// </summary>
        [Key(5)]
        public ActorLocationState Stated { get; set; }
        /// <summary>
        /// 服务通讯管道Id
        /// </summary>
        [IgnoreMember]
        internal int ChannelId { get; set; }
    }
}
