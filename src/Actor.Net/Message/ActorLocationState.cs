﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// Actor位置信息远程调用状态
    /// </summary>
    public enum ActorLocationState
    {
        /// <summary>
        /// 调用成功
        /// </summary>
        Successful = 0,
        /// <summary>
        /// Actor服务分组位置信息不存在
        /// </summary>
        GroupLocationNotExisted = 1,
        /// <summary>
        /// Actor类型位置信息不存在
        /// </summary>
        ActorTypeLocationNotExisted = 2,
        /// <summary>
        /// Actor位置信息不存在
        /// </summary>
        ActorLocationNotExisted = 3,
        /// <summary>
        /// Actor服务分组不存在
        /// </summary>
        GroupServerNotExisted = 4,
    }
}
