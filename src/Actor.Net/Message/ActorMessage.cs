﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// Actor请求消息实体
    /// </summary>
    [MessagePackObject]
    public class ActorMessageRequest
    {
        /// <summary>
        /// Actor服务分组
        /// </summary>
        [Key(0)]
        public string ServerGroup { get; set; }
        /// <summary>
        /// Actor类型
        /// </summary>
        [Key(1)]
        public string ActorType { get; set; }
        /// <summary>
        /// Actor Id
        /// </summary>
        [Key(2)]
        public string ActorId { get; set; }
        /// <summary>
        /// Actor路由
        /// </summary>
        [Key(3)]
        public string Router { get; set; }
    }

    /// <summary>
    /// Actor返回消息实体
    /// </summary>
    [MessagePackObject]
    public class ActorMessageResponse
    {
        /// <summary>
        /// Actor调用结果状态
        /// </summary>
        [Key(0)]
        public ActorCallState Stated { get; set; }
        /// <summary>
        /// 远程服务异常堆栈信息
        /// </summary>
        [Key(1)]
        public string ExceptionInfo { get; set; }
    }
}
