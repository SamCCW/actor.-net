﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// 分组信息
    /// </summary>
    [MessagePackObject]
    public class GroupInfo
    {
        /// <summary>
        /// Actor服务组
        /// </summary>
        [Key(0)]
        public string ServerGroup { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        [Key(1)]
        public string IpAddress { get; set; }
        /// <summary>
        /// 服务监听端口
        /// </summary>
        [Key(2)]
        public int Port { get; set; }
        /// <summary>
        /// 通讯管道Id
        /// </summary>
        [IgnoreMember]
        internal int ChannelId { get; set; }
    }
}
