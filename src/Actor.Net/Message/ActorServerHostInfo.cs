﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Actor.Net.Message
{
    /// <summary>
    /// Actor服务主机信息
    /// </summary>
    [MessagePackObject]
    public class ActorServerHostInfo
    {
        /// <summary>
        /// Ip地址
        /// </summary>
        [Key(0)]
        public string IpAddress { get; set; }
        /// <summary>
        /// 监听端口
        /// </summary>
        [Key(1)]
        public int Port { get; set; }
    }
}
