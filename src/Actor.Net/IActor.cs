﻿using Actor.Net;
using Actor.Net.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actor.Net
{
    /// <summary>
    /// Actor接口，所有Actor接口类都必须继承该接口。
    /// 对于Actor的接口方法，有严格规定，必须只有一个参数，返回值为void、Task、Task泛型三种。这样严格规定的方法确实有灵活性问题，但是可以让代码风格更统一。
    /// </summary>
    public interface IActor
    {
        /// <summary>
        /// 服务分组
        /// </summary>
        string ServerGroup { get; }
        /// <summary>
        /// Actor类型
        /// </summary>
        string ActorType { get; }
        /// <summary>
        /// ActorId
        /// </summary>
        string ActorId { get; }
        /// <summary>
        /// 是否是本地服务
        /// </summary>
        bool IsLocal { get; set; }
        /// <summary>
        /// 位置信息上下文
        /// </summary>
        LocationContext LocationContext { get; set; }
        /// <summary>
        /// AOP代理信息
        /// </summary>
        ActorProxyInfo ProxyInfo { get; set; }
    }
}
