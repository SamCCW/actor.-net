﻿using Actor.Net;
using First.ActorDesign;
using System;

namespace First.ActorServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ActorProviderService.CreateProviderService("127.0.0.1", 48798, "127.0.0.1", 21112)
                .BindActor(typeof(IFirstActor).Assembly)
                .AddExceptionHandler((cx, ex) => Console.WriteLine(ex.ToString()))
                .AddActorStartHandler(async () =>
                {
                    await ActorProviderService.AddActor<IFirstActor>();
                })
                .Start(true);

            Console.ReadKey();
        }
    }
}
