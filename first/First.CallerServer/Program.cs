﻿using Actor.Net;
using First.ActorDesign;
using First.Modles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace First.CallerServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //下面是创建一个Actor提供服务的完整过程            
            ActorProviderService.CreateProviderService("127.0.0.1", 48798,
                "127.0.0.1", 21111)
                .BindActor(typeof(IFirstActor).Assembly)
                .AddExceptionHandler((cx, ex) => Console.WriteLine(ex.ToString()))
                .AddActorStartHandler(() =>
                {
                    SendActorMessage();
                })
                .Start(true);

            Console.ReadKey();
        }

        private static async void SendActorMessage()
        {
            var sendParameter = new FirstParameter
            {
                One = "One参数内容",
                Two = "Two参数内容",
            };

            //延迟2秒等待First.ActorServer注册
            await Task.Delay(2000);

            //在Actor.Net框架中只需要通过ActorProviderService.GetActor<TActor>()获取到Actor实例，就可以
            //调用Actor实例中的方法与远程服务通讯了，这样的编码方式好处是显而易见的，不需要任何的接口文
            //档开发人员就可以了解自己需要调用的服务接口参数，可以省下非常大的沟通成本与编码成本，团队
            //无论水平如何，都能很好的保持一致的代码风格。

            var actor = await ActorProviderService.GetActor<IFirstActor>();
            if(actor == null)
            {
                Console.WriteLine("Actor提供服务没有启动");
            }

            //测试同步发送方法
            actor.SendActorParameter(sendParameter);

            //测试异步发送方法
            await actor.SendActorParameterAsync(sendParameter);

            //测试异步调用方法
            var receiveParameter = await actor.GetActorParameterAsync(sendParameter);
            Console.WriteLine($"ActorServer 返回结果 {receiveParameter.One} {receiveParameter.Two}");

        }
    }
}
