﻿using Actor.Net;
using First.Modles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace First.ActorDesign
{
    public class FirstActor : ActorBase, IFirstActor
    {
        [Func(typeof(ActorParameter<FirstParameter, FirstParameter>))]
        public Task<FirstParameter> GetActorParameterAsync(FirstParameter parameter)
        {
            return Task.FromResult(parameter); 
        }

        [Func(typeof(ActorParameter<FirstParameter>))]
        public void SendActorParameter(FirstParameter parameter)
        {
            Console.WriteLine($"SendActorParameter 传入 {parameter.One} {parameter.Two}");
        }

        [Func(typeof(ActorParameter<FirstParameter>))]
        public Task SendActorParameterAsync(FirstParameter parameter)
        {
            Console.WriteLine($"SendActorParameterAsync 传入 {parameter.One} {parameter.Two}");
            return Task.CompletedTask;
        }
    }
}
