﻿using Actor.Net;
using First.Modles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace First.ActorDesign
{
    public interface IFirstActor : IActor
    {
        void SendActorParameter(FirstParameter parameter);
        Task SendActorParameterAsync(FirstParameter parameter);
        Task<FirstParameter> GetActorParameterAsync(FirstParameter parameter);
    }
}
