﻿using Actor.Net;
using First.ActorDesign;
using First.Modles;
using System;
using System.Diagnostics;

namespace First.Benchmark
{
    class Program
    {
        //Actor.Net是一个高性能的分布式服务框架，单线程Socket，支持多路复用，低GC开销，低CPU开销。
        //QPS性能可以达到100000/s，RPS可以达到50000/s，这只是在7代i5笔记本上测试结果，并且在满载
        //的情况下，还能保持非常低的CPU占用。

        //启动该服务测试需要启动Location.Service与First.ActorServer

        static void Main(string[] args)
        {
            //下面是创建一个Actor提供服务的完整过程            
            ActorProviderService.CreateProviderService("127.0.0.1", 48798,
                "127.0.0.1", 21111)
                .BindActor(typeof(IFirstActor).Assembly)
                .AddExceptionHandler((cx, ex) => Console.WriteLine(ex.ToString()))
                .AddActorStartHandler(() =>
                {
                    StartBenchmark();
                })
                .Start(true);

            Console.ReadKey();
        }

        //当前完成Actor调用的总次数
        static int completeCount = 0;

        static void StartBenchmark()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            //开启200条协程同时发起服务调用
            for (var i=0; i< 200; i++)
                BenchmarkWork(stopwatch);
        }

        static async void BenchmarkWork(Stopwatch stopwatch)
        {
            var sendParameter = new FirstParameter
            {
                One = "One参数内容",
                Two = "Two参数内容",
            };

            var actor = await ActorProviderService.GetActor<IFirstActor>();
            if (actor == null)
            {
                Console.WriteLine("请检查Location.Service与First.ActorServer是否启动");
                Console.ReadKey();
                return;
            }


            while (true)
            {
                //发起服务调用
                var receiveParameter = await actor.GetActorParameterAsync(sendParameter);
                if(receiveParameter == null)
                {
                    Console.WriteLine("出错了...");
                    Console.ReadKey();
                }
                completeCount++;

                //打印每完成10万次服务调用的耗时
                if(completeCount % 100000 == 0)
                {
                    Console.WriteLine($"100000次服务调用耗时{stopwatch.ElapsedMilliseconds}毫秒，总完成服务调用次数{completeCount}");
                    stopwatch.Restart();
                }
            }

        }
    }
}
