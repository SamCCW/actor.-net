﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace First.Modles
{
    [MessagePackObject]
    public class FirstParameter
    {
        [Key(0)]
        public string One { get; set; }

        [Key(1)]
        public string Two { get; set; }
    }
}
