# Actor.Net

#### 介绍
一、Actor.Net是一款高性能的服务端微服务RPC框架<br>
    底层Socket是单线程设计，单服务在本地unix网络环境中测试可以达到100000 TPS、50000 QPS吞吐量。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0924/145859_8da726a6_1415598.png "屏幕截图.png")
二、像调用本地方法一样调用远程服务<br>
    该框架设计其中一个核心是为了降低分布式系统开发沟通成本，在做微服务开发过程中，服务之间调用依赖于接口，一般的Socket接口设计基本上离不开消息总线Id，消息内容，这种设计开发人员需要定义大量的消息总线指令Id,接口提供服务需要对不同的消息总线指令Id进行解包反序列化处理，消息总线的代码需要放在一个大冗余的switch case块中处理，如果与其他开发人员对接时会带来很大的不便，一般接口提供方都需要提供一份详细接口文档给调用方，这种开发方式无疑沟通成本不低。Actor.Net内部实现消息路由管理，无需编写大量的消息总线指令Id,无需自己解包反序列化处理，接口实现与写本地函数一模一样，有点类似于.net mvc框架，接口调用与调用本地函数一模一样，接口开发人员只需要把接口的库共享给接口调用开发人员，调用开发人员就就可以使用服务提供的接口，基本上省去双方很大的沟通成本。

三、编码规范<br>
    Actor.Net提供一个编写Actor接口组件Actor.Analyzer，可以帮助修正开发过程中错误代码及更好的规范接口的代码。

四、学习成本<br>
    相对于一些其他的服务端通讯框架，Actor.Net学习成本非常低，因为Actor.Net仅仅是提供一个简单Actor通讯模型框架。

五、不支持跨语言<br>
    Actor.Net不是一个通用框架，因为内部是使用反射获取每个Actor方法进行路由绑定，所以接口路由很难被其他语言适配，只适合用.net core框架开发。

六、Actor.Net适用场景<br>
    Actor.Net适合游戏、web等分布式系统服务端项目，它只是一款针对C#语言特性开发的RPC框架，适合用于任何.net微服务系统RPC通讯，Actor.Net提供了一个GateNetwork组件用于C/S类产品开发，GateNetwork提供tcp与websocket两种协议支持。

七、开源申明<br>
    开源目的只为了学习交流，绝非商业行为，只为在互联网行业留下一个今日足迹。

#### 软件架构
    Actor.Net框架采用的是.net standard2.0开发，只支持.net core 2.2或者.net framework更高版本。
    服务端通讯库协议采用的是MessagePack，GateNetwork组件支持protobuf与messagePack数据包协议。
    基于运行时AOP拦截实现，依赖于Castle.Core AOP三方组件。

#### 安装教程

1.  安装Actor.Net核心组件
Install-Package Actor.Net -Version 1.0.0

2.  安装Actor.Net代码分析组件
Install-Package Actor.Analyzer -Version 1.0.1

#### 使用Actor.Net创建第一个分布式服务

1.  Actor消息协议项目<br>
创建一个.net standard名为First.Modles项目，安装Actor.Net,创建一个类FirstParameter，放入如下代码
```
using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace First.Modles
{
    [MessagePackObject]
    public class FirstParameter
    {
        [Key(0)]
        public string One { get; set; }

        [Key(1)]
        public string Two { get; set; }
    }
}
```

2.  说明：Actor设计实现项目<br>
创建一个.net standard名为First.ActorDesign项目，引用First.Modles项目，创建一个接口IFirstActor，放入如下代码

```
using Actor.Net;
using First.Modles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace First.ActorDesign
{
    public interface IFirstActor : IActor
    {
        void SendActorParameter(FirstParameter parameter);
        Task SendActorParameterAsync(FirstParameter parameter);
        Task<FirstParameter> GetActorParameterAsync(FirstParameter parameter);
    }
}
```
创建一个类FirstActor，放入如下代码

```
using Actor.Net;
using First.Modles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace First.ActorDesign
{
    public class FirstActor : ActorBase, IFirstActor
    {
        [Func(typeof(ActorParameter<FirstParameter, FirstParameter>))]
        public Task<FirstParameter> GetActorParameterAsync(FirstParameter parameter)
        {
            return Task.FromResult(parameter); 
        }

        [Func(typeof(ActorParameter<FirstParameter>))]
        public void SendActorParameter(FirstParameter parameter)
        {
            Console.WriteLine($"SendActorParameter 传入 {parameter.One} {parameter.Two}");
        }

        [Func(typeof(ActorParameter<FirstParameter>))]
        public Task SendActorParameterAsync(FirstParameter parameter)
        {
            Console.WriteLine($"SendActorParameterAsync 传入 {parameter.One} {parameter.Two}");
            return Task.CompletedTask;
        }
    }
}
```

3.  Actor位置服务服务端，Actor服务注册、Actor服务发现、Actor生命周期管理。<br>
创建一个.net core控制台名为Location.Service项目，安装Actor.Net，放入如下代码

```
using Actor.Net;
using System;

namespace Location.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            ActorLocationServer.ListenPort = 48798;
            ActorLocationServer.Instance.ExceptionHandler = (cx, ex) => Console.WriteLine(ex.ToString());
            ActorLocationServer.Instance.Start(true);
            Console.ReadKey();
        }
    }
}
```

4.  Actor宿主服务<br>
创建一个.net core控制台名为First.ActorServer项目，引用First.ActorDesign项目，放入如下代码

```
using Actor.Net;
using First.ActorDesign;
using System;

namespace First.ActorServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ActorProviderService.CreateProviderService("127.0.0.1", 48798, "127.0.0.1", 21112)
                .BindActor(typeof(IFirstActor).Assembly)
                .AddExceptionHandler((cx, ex) => Console.WriteLine(ex.ToString()))
                .AddActorStartHandler(async () =>
                {
                    await ActorProviderService.AddActor<IFirstActor>();
                })
                .Start(true);

            Console.ReadKey();
        }
    }
}
```

5.  Actor调用服务<br>
创建一个.net core控制台名为First.CallerServer项目，引用First.ActorDesign项目，放入如下代码

```
using Actor.Net;
using First.ActorDesign;
using First.Modles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace First.CallerServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //下面是创建一个Actor提供服务的完整过程            
            ActorProviderService.CreateProviderService("127.0.0.1", 48798,
                "127.0.0.1", 21111)
                .BindActor(typeof(IFirstActor).Assembly)
                .AddExceptionHandler((cx, ex) => Console.WriteLine(ex.ToString()))
                .AddActorStartHandler(() =>
                {
                    SendActorMessage();
                })
                .Start(true);

            Console.ReadKey();
        }

        private static async void SendActorMessage()
        {
            var sendParameter = new FirstParameter
            {
                One = "One参数内容",
                Two = "Two参数内容",
            };

            //延迟2秒等待First.ActorServer注册
            await Task.Delay(2000);

            //在Actor.Net框架中只需要通过ActorProviderService.GetActor<TActor>()获取到Actor实例，就可以
            //调用Actor实例中的方法与远程服务通讯了，这样的编码方式好处是显而易见的，不需要任何的接口文
            //档开发人员就可以了解自己需要调用的服务接口参数，可以省下非常大的沟通成本与编码成本，团队
            //无论水平如何，都能很好的保持一致的代码风格。

            var actor = await ActorProviderService.GetActor<IFirstActor>();
            if(actor == null)
            {
                Console.WriteLine("Actor提供服务没有启动");
            }

            //测试同步发送方法
            actor.SendActorParameter(sendParameter);

            //测试异步发送方法
            await actor.SendActorParameterAsync(sendParameter);

            //测试异步调用方法
            var receiveParameter = await actor.GetActorParameterAsync(sendParameter);
            Console.WriteLine($"ActorServer 返回结果 {receiveParameter.One} {receiveParameter.Two}");

        }
    }
}

```

6.  Visual studio工程属性选多启动项目，勾选启动Location.Service、First.ActorServer、First.ActorCaller，F5启动如无意外你就可以看到
First.ActorServer会提示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0923/124040_ed8295bb_1415598.png "屏幕截图.png")
First.ActorCaller会提示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0923/124126_10549eac_1415598.png "屏幕截图.png")

至此一个简单的分布式服务创建成功。

#### 参与贡献

1.  邮箱地址zjccwboy@yeah.net
2.  QQ 70380130


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
