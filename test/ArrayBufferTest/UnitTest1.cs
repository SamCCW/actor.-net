using Actor.Net.Network;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ArrayBufferTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var writeString = TestContent.TestString;
            var testBytes = System.Text.Encoding.UTF8.GetBytes(writeString);

            var arrayBuffer = new ArrayBuffer();

            //写到发送缓冲区中
            arrayBuffer.Write(testBytes, 0, testBytes.Length);

            //从发送缓冲区读取字节数组
            var sendBytes = arrayBuffer.Get(out int count);

            //校验写到缓冲区与在缓冲区读取出来的数据是否一致
            var readString = System.Text.Encoding.UTF8.GetString(sendBytes, 0, count);

            //校验写入缓冲区数据与读出的数据是否一致
            Assert.AreEqual(writeString, readString);
        }
    }
}
